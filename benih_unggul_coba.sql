-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 20, 2019 at 09:24 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `benih_unggul_coba`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`, `foto`) VALUES
(1, 'auwfar', 'f0a047143d1da15b630c73f0256d5db0', 'Achmad Chadil Auwfar', 'Koala.jpg'),
(2, 'ozil', 'f4e404c7f815fc68e7ce8e3c2e61e347', 'Mesut ', 'profil2.jpg'),
(3, 'okta', '202cb962ac59075b964b07152d234b70', 'Oktaviansyah', 'profil1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `benih`
--

CREATE TABLE `benih` (
  `id_benih` int(11) NOT NULL,
  `komoditi` varchar(30) NOT NULL,
  `varietas_klon` varchar(30) NOT NULL,
  `bulan_tanam` varchar(5) NOT NULL,
  `tinggi` float NOT NULL,
  `jumlah_daun` int(11) NOT NULL,
  `akhir_masa_edar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `benih`
--

INSERT INTO `benih` (`id_benih`, `komoditi`, `varietas_klon`, `bulan_tanam`, `tinggi`, `jumlah_daun`, `akhir_masa_edar`) VALUES
(167, 'Padi', 'Komasti 580', '02', 14, 16, '2019-12-31'),
(169, 'Teh', 'Komasti 760', '01', 18, 19, '2019-12-31'),
(178, 'KOM 12', 'K', '01', 234, 23, '2019-12-31'),
(179, 'KOM 3', 'SA', '01', 23, 23, '2019-12-31'),
(180, 'KOM TES EDIT', 'EDI', '01', 23, 23, '2019-12-31'),
(181, 'tes kom2', 'v', '01', 23, 23, '2019-12-31'),
(182, 'ref', 'ref', '09', 32, 32, '2019-03-18'),
(183, 'bafu aja', 'Komasti 320', '08', 13, 3, '2019-03-19'),
(184, 'Komoditas Kelapa Sawit', 'Komasti 320', '08', 13, 3, '2019-03-19'),
(185, 'Komoditas Kelapa Sawit', 'Komasti 320', '08', 13, 3, '2019-03-19'),
(186, 'Komoditas Baru Ini', 'Komasti 320', '11', 12.75, 13, '2019-03-19'),
(187, 'rett', 'fdg', '01', 23, 8, '2019-12-31'),
(188, 'YAN GGGG BARU NEH', 'SAJOP', '01', 89, 89, '2019-12-31'),
(190, 'babababa', 'sadsa', '10', 2323, 3232, '2019-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `label`
--

CREATE TABLE `label` (
  `id_label` int(11) NOT NULL,
  `jenis_benih` varchar(40) NOT NULL,
  `warna` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `label`
--

INSERT INTO `label` (`id_label`, `jenis_benih`, `warna`) VALUES
(10, 'Benih Penjenis (BS)', 'Kuning'),
(11, 'Benih Dasar (BD)', 'Putih'),
(12, 'Benih Pokok (BP)', 'Ungu'),
(13, 'Benih Sebar (BR) Unggul', 'Nila'),
(14, 'Benih Sebar (BR) Lokal', 'Hijau');

-- --------------------------------------------------------

--
-- Table structure for table `produsen`
--

CREATE TABLE `produsen` (
  `id_produsen` int(11) NOT NULL,
  `npwp` varchar(20) NOT NULL,
  `nama_perusahaan` varchar(40) NOT NULL,
  `pimpinan` varchar(60) NOT NULL,
  `alamat_perusahaan` varchar(60) NOT NULL,
  `jenis_usaha` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produsen`
--

INSERT INTO `produsen` (`id_produsen`, `npwp`, `nama_perusahaan`, `pimpinan`, `alamat_perusahaan`, `jenis_usaha`) VALUES
(14, '9332134542', 'PT Perkebunan Kita', 'Muqsith', 'Jl. Kebun Bersama', 'Pertanian'),
(15, '9324339238', 'PT Kita Bahagia', 'Tio', 'Jl. Bahagia Bersama', 'Kebahagiaan'),
(16, '9201842219', 'PT Jaya Selalu', 'Dimas', 'Jl. Jaya Selalu', 'Usaha Berjaya');

-- --------------------------------------------------------

--
-- Table structure for table `qrcode`
--

CREATE TABLE `qrcode` (
  `id_qrcode` int(11) NOT NULL,
  `id_sertifikat` int(11) NOT NULL,
  `id_label` int(11) NOT NULL,
  `volume` int(11) NOT NULL,
  `hasil_lapang` varchar(50) NOT NULL,
  `tgl` date NOT NULL,
  `foto_qrcode` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qrcode`
--

INSERT INTO `qrcode` (`id_qrcode`, `id_sertifikat`, `id_label`, `volume`, `hasil_lapang`, `tgl`, `foto_qrcode`) VALUES
(130, 179, 10, 2, 'SR.120.2019', '2019-03-20', '7e1a04ee8d6317ce879e90132e4b729f.png');

-- --------------------------------------------------------

--
-- Table structure for table `sertifikat`
--

CREATE TABLE `sertifikat` (
  `id_sertifikat` int(11) NOT NULL,
  `sert_sumber_benih` int(11) DEFAULT NULL,
  `no_sertifikat` varchar(35) NOT NULL,
  `id_produsen` int(11) NOT NULL,
  `id_benih` int(11) NOT NULL,
  `pengawas` varchar(30) NOT NULL,
  `masa_berlaku` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sertifikat`
--

INSERT INTO `sertifikat` (`id_sertifikat`, `sert_sumber_benih`, `no_sertifikat`, `id_produsen`, `id_benih`, `pengawas`, `masa_berlaku`) VALUES
(179, NULL, 'SERT 1', 15, 178, '    PEN', '2019-12-31'),
(180, 179, 'SERT 2', 14, 179, '  PENG', '2019-12-31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `benih`
--
ALTER TABLE `benih`
  ADD PRIMARY KEY (`id_benih`);

--
-- Indexes for table `label`
--
ALTER TABLE `label`
  ADD PRIMARY KEY (`id_label`);

--
-- Indexes for table `produsen`
--
ALTER TABLE `produsen`
  ADD PRIMARY KEY (`id_produsen`);

--
-- Indexes for table `qrcode`
--
ALTER TABLE `qrcode`
  ADD PRIMARY KEY (`id_qrcode`),
  ADD KEY `id_label` (`id_label`),
  ADD KEY `id_sertifikat` (`id_sertifikat`);

--
-- Indexes for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD PRIMARY KEY (`id_sertifikat`),
  ADD KEY `id_produsen` (`id_produsen`),
  ADD KEY `id_benih` (`id_benih`),
  ADD KEY `sert_sumber_benih` (`sert_sumber_benih`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `benih`
--
ALTER TABLE `benih`
  MODIFY `id_benih` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `label`
--
ALTER TABLE `label`
  MODIFY `id_label` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `produsen`
--
ALTER TABLE `produsen`
  MODIFY `id_produsen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `qrcode`
--
ALTER TABLE `qrcode`
  MODIFY `id_qrcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `sertifikat`
--
ALTER TABLE `sertifikat`
  MODIFY `id_sertifikat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `qrcode`
--
ALTER TABLE `qrcode`
  ADD CONSTRAINT `qrcode_ibfk_2` FOREIGN KEY (`id_label`) REFERENCES `label` (`id_label`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `qrcode_ibfk_3` FOREIGN KEY (`id_sertifikat`) REFERENCES `sertifikat` (`id_sertifikat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sertifikat`
--
ALTER TABLE `sertifikat`
  ADD CONSTRAINT `sertifikat_ibfk_1` FOREIGN KEY (`id_produsen`) REFERENCES `produsen` (`id_produsen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sertifikat_ibfk_2` FOREIGN KEY (`id_benih`) REFERENCES `benih` (`id_benih`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sertifikat_ibfk_3` FOREIGN KEY (`sert_sumber_benih`) REFERENCES `sertifikat` (`id_sertifikat`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
